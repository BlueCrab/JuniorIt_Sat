from random import *
import pickle

hp = 20  # imbalance
weapon = {'Stick' : 2}
dmg = 1 + list(weapon.values())[0]
score = 0

print('Hello traveler!')
while hp > 0:
    choice = input('Choose the door.')
    choice = choice.lower()
    if choice == 'left' or choice == 'right' or choice == 'forward':
        luck = random()*4
        if luck <= 1:
            print('MONSTER!')
            mhp = 5
            mdmg = 2
            while True:
                mhp -= dmg
                print('You hit moster! Monster hp:', mhp)
                if mhp <= 0:
                    print('Monster slained!')
                    score += 1
                    print('Weapon dropped!')
                    pick = input('Pick up?')
                    if pick.lower() == 'yes':
                        weapon = {'Knife': 300}
                    else:
                        print('Oke :c')
                    break
                hp -= mdmg
                print('Monster hit you! Your hp: ', hp)
        elif 1 < luck < 3:
            print('Path is clear')
        else:
            hp += 1
            print('You foun a shrine. HEALED! Your hp:', hp)

    elif choice == 'stats':
        print('Hp:', hp, 'Weapon:', weapon, 'Score:', score)

    elif choice == 'save':
        with open('save.bin', 'wb') as f:
            save = [hp, weapon, score]
            pickle.dump(save, f)

    elif choice == 'load':
        with open('save.bin', 'rb') as f:
            save = pickle.load(f)
            hp = save[0]
            weapon = save[1]
            score = save[2]
